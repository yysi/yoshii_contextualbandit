import numpy as np


# シグモイド関数
def sigmoid(x):
    return 0.5 * np.tanh(0.5 * x) + 0.5


"""
    配列の要素数
    k=n_arms
    f=n_features
"""


# バンディットのベースとなるクラス
class BaseBandit(object):
    def __init__(self, n_arms=5, n_features=15, scale=0.1, noise=0.1):
        self.n_arms = n_arms  # 腕(選択肢)の数
        self.n_features = n_features  # 特徴量の次元数
        self.scale = scale  # 報酬の分散
        self.noise = noise  # 誤差項の分散
        self.steps = 0
        self.best_arm = 0  # 本来、最も報酬の期待値が高い腕のインデックス

        """ 
        平均(行列n_dim*n_features)0, 分散共分散行列(scale*(n_features*n_features)の単位行列), データサイズn_armsとしたときの転置行列
        例:Artworkなどの選択肢
        """
        self.arm_params = np.random.multivariate_normal(
            np.zeros(self.n_features),
            self.scale * np.identity(self.n_features),
            size=self.n_arms).T  # f*k

    def initialize(self):
        self.steps = 0
        self.arm_params = np.random.multivariate_normal(
            np.zeros(self.n_features),
            self.scale * np.identity(self.n_features), size=self.n_arms).T

    def update_params(self):
        self.steps += 1


# 特徴量を踏まえ、報酬を正規分布から観測した連続的な実数値で返す
class GaussianBandit(BaseBandit):
    def __init__(self, n_arms=5, n_features=15, scale=0.1, noise=0.1):
        super().__init__(n_arms, n_features, scale, noise)

    # 腕を選択したユーザーの特徴量をもとに、報酬・リグレット・引くべき腕のインデックスを返す
    def update_params(self, x=None, chosen_arm=None):
        x = np.expand_dims(x, axis=1)  # 特徴量の次元を変更
        # e: 誤差項
        e = np.random.normal(loc=0, scale=self.noise)
        # mu: 真のパラメータの期待値
        mu = np.ravel(self.arm_params.T @ x)
        reward = np.random.normal(loc=mu[chosen_arm] + e, scale=self.scale)
        regret = np.max(mu) - mu[chosen_arm]
        self.best_arm = np.argmax(mu)

        return reward, regret, self.best_arm


# 特徴量を踏まえ、報酬を二項分布から観測した離散値で返す
class BernoulliBandit(BaseBandit):
    def __init__(self, n_arms=5, n_features=15, scale=0.1, noise=0.1):
        super().__init__(n_arms, n_features, scale, noise)

    # 腕を選択したユーザーの特徴量をもとに、報酬・リグレット・引くべき腕のインデックスを返す
    def update_params(self, x=None, chosen_arm=None):
        x = np.expand_dims(x, axis=1)
        # e: 誤差項
        e = np.random.normal(loc=0, scale=self.noise)
        # mu: 真のパラメータの期待値
        mu = np.ravel(self.arm_params.T @ x)  # 1*k
        reward = np.random.binomial(n=1, p=sigmoid(mu[chosen_arm] + e))
        regret = np.max(mu) - mu[chosen_arm]
        self.best_arm = np.argmax(mu)

        return reward, regret, self.best_arm

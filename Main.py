import numpy as np
import matplotlib.pyplot as plt
import time
import Environment as ev
import Agent as ag


N_ARMS = 5  # 腕(選択肢)の数
N_SIMS = 50  # シミュレーション回数
N_STEPS = 15000  # 1シミュレーションの試行回数

N_FEATURES = 15  # 特徴量の次元数


def plot_graph(data, labels, file_name, xlabel="Steps", ylabel="Accuracy"):
    plt.figure(figsize=(12, 8))
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    for i, graph in enumerate(data):
        plt.plot(graph, label=labels[i], alpha=0.8, linewidth=1.2)
    plt.legend(loc="upper left")
    plt.savefig(file_name)
    # plt.show()
    plt.clf()


def main():
    """
    ダミー変数を利用したデータの作成
    文脈を0か1の離散値とし、各ユーザーについて設定した次元(N_DIM)の文脈が観測される。
    """
    data = [np.concatenate([np.expand_dims(np.random.randint(2, size=N_FEATURES), axis=1) for i in np.arange(N_STEPS)], axis=1) for j in np.arange(N_SIMS)]  # N_SIMS*N_FEATURES*N_STEPS

    """ 環境であるバンディットの生成 """
    # ガウシアン分布に従った連続的な報酬を返すバンディット
    # env = ev.GaussianBandit(n_arms=N_ARMS, n_features=N_FEATURES, scale=0.1, noise=0.1)
    # ベルヌーイ分布に従った離散的な報酬を返すバンディット
    env = ev.BernoulliBandit(n_arms=N_ARMS, n_features=N_FEATURES, scale=0.1, noise=0.1)

    """ エージェントを生成 """
    # agent_list = [ag.LinUCB(n_arms=N_ARMS, n_features=N_FEATURES, warm_up=1, batch_size=300, alpha=0.1)]
    # agent_list = [ag.LinTS(n_arms=N_ARMS, n_features=N_FEATURES, warm_up=1, batch_size=15, noise=0.1)]
    # agent_list = [ag.LinRS(n_arms=N_ARMS, n_features=N_FEATURES, warm_up=1, batch_size=300, aleph=0.8)]
    agent_list = [
        ag.LinUCB(n_arms=N_ARMS, n_features=N_FEATURES, warm_up=1, batch_size=300, alpha=0.1),
        ag.LinTS(n_arms=N_ARMS, n_features=N_FEATURES, warm_up=1, batch_size=15, noise=0.1),
        ag.LinRS(n_arms=N_ARMS, n_features=N_FEATURES, warm_up=1, batch_size=300, aleph=0.8)]
    n_agents = len(agent_list)

    accuracy_graph = np.zeros((n_agents, N_STEPS))
    reward_graph = np.zeros((n_agents, N_STEPS))
    regret_graph = np.zeros((n_agents, N_STEPS))

    start_time = time.time()
    """ シミュレーション開始 """
    # 指定したエージェント毎に試行
    for i, agent in enumerate(agent_list):
        # パラメータN_SIMS回分のシミュレーション
        for sim in range(N_SIMS):
            env.initialize()  # 環境データの初期化
            agent.initialize()  # エージェントデータの初期化
            sum_rewards = 0.0
            sum_regret = 0.0
            # パラメータN_STEPS回分の試行を繰り返す
            for step in range(N_STEPS):
                x = data[sim][:, step]
                chosen_arm = agent.select_arm(x)  # 腕を選択
                reward, regret, best_arm = env.update_params(x, chosen_arm)  # 環境から、特徴データと選んだ腕をもとに報酬を得る
                agent.update(chosen_arm, x, reward)  # 報酬をもとにエージェントの持つパラメータを更新

                # 現stepで選択された腕が、真の期待値が最も高い腕であるかどうかの真偽をインクリメント
                accuracy_graph[i, step] += 1 if chosen_arm == best_arm else 0
                sum_rewards += reward
                reward_graph[i, step] += reward
                sum_regret += regret
                regret_graph[i, step] += sum_regret
            print(agent.__class__.__name__, sim, ":", sum_regret)

    elapsed_time = time.time() - start_time

    accuracy_graph /= N_SIMS
    reward_graph /= N_SIMS
    regret_graph /= N_SIMS

    # labels = ["LinUCB"]
    # file_name = "LinUCB regret"
    # plot_graph(regret_graph, labels, file_name, ylabel="Average Regret")

    labels = ["LinUCB", "LinTS", "LinRS"]
    file_name = "reward"
    plot_graph(reward_graph, labels, file_name, ylabel="Average reward")

    file_name = "accuracy"
    plot_graph(accuracy_graph, labels, file_name, ylabel="Accuracy")

    file_name = "regret"
    plot_graph(regret_graph, labels, file_name, ylabel="Average Regret")

    print(f"経過時間: {elapsed_time}")


if __name__ == '__main__':
    main()

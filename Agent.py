import numpy as np

"""
    配列の要素数
    k=n_arms
    f=n_features
"""


# ベースとなるエージェントクラス
class Agent(object):
    def __init__(self, n_arms=5, n_features=15, warm_up=1, batch_size=300):
        self.n_arms = n_arms  # 腕(選択肢)の数
        self.n_features = n_features  # 特徴量の次元
        self.steps = 0  # 現在のstep数
        self.q = np.zeros(n_arms)  # 各腕の価値
        self.counts = np.zeros(n_arms)  # 各腕が選択された回数を保存
        self.warm_up = warm_up  # 知識活用を始めるまでのそれぞれの腕の探索数
        self.batch_size = batch_size  # 次のパラメータ更新をするまでのstep数

    # パラメータの初期化
    def initialize(self):
        self.steps = 0
        self.counts = np.zeros_like(self.counts)

    # パラメータの更新
    def update(self):
        self.steps += 1


class LinUCB(Agent):
    def __init__(self, n_arms=5, n_features=15, warm_up=1, batch_size=300, alpha=0.1):
        super().__init__(n_arms, n_features)
        self.alpha = alpha
        self.theta_hat = np.zeros((n_features, n_arms))  # d*k

        self.A_inv = np.concatenate(
            [np.identity(n_features) for i in np.arange(n_arms)]).reshape(n_arms,
                                                                          n_features,
                                                                          n_features)  # k*f*f 腕ごとのインプレッション数
        self.b = np.zeros((n_features, n_arms))  # f*k 腕ごとのクリック数
        self._A_inv = np.concatenate(
            [np.identity(n_features) for i in np.arange(n_arms)]).reshape(n_arms,
                                                                          n_features,
                                                                          n_features)  # k*f*f
        self._b = np.zeros((n_features, n_arms))  # f*k

    def initialize(self):
        super().initialize()
        self.theta_hat = np.zeros_like(self.theta_hat)  # f*k
        self.A_inv = np.concatenate(
            [np.identity(self.n_features) for i in np.arange(self.n_arms)]).reshape(
            self.n_arms, self.n_features, self.n_features)
        self.b = np.zeros_like(self.b)
        self._A_inv = np.concatenate(
            [np.identity(self.n_features) for i in np.arange(self.n_arms)]).reshape(
            self.n_arms, self.n_features, self.n_features)
        self._b = np.zeros_like(self._b)

    # 特徴量をもとに現時点での最適な腕を選択し、そのインデックスを返す
    def select_arm(self, x):
        # 全ての腕がwarm_up回選択されるまで
        if self.steps < self.warm_up * self.n_arms:
            unexploited_idx = np.where(self.counts < self.warm_up)
            result = np.random.choice(unexploited_idx[0])
        else:
            x = np.expand_dims(x, axis=1)  # 特徴量の次元を変更
            self.theta_hat = np.concatenate(
                [self.A_inv[i] @ np.expand_dims(self.b[:, i], axis=1) for i in
                 np.arange(self.n_arms)],
                axis=1)  # f*k CTR=クリック率/インプレッション数
            sigma_hat = np.concatenate(
                [np.sqrt(x.T @ self.A_inv[i] @ x) for i in
                 np.arange(self.n_arms)], axis=1)  # 1*k
            result = np.argmax(
                x.T @ self.theta_hat + self.alpha * sigma_hat)  # CTR+α(1/インプレッション数)^(1/2)

        return result

    # アルゴリズムが持つパラメータを更新する(batch_size回ごと)
    def update(self, chosen_arm=None, x=None, reward=None):
        super().update()
        self.counts[chosen_arm] += 1
        x = np.expand_dims(x, axis=1)  # 特徴量の次元を変更

        self._A_inv[chosen_arm] -= self._A_inv[chosen_arm] @ x @ x.T @ \
                                   self._A_inv[chosen_arm] / (
                                               1 + x.T @ self._A_inv[
                                           chosen_arm] @ x)
        self._b[:, chosen_arm] += np.ravel(x) * reward  # f*1
        # batch_size回ごとに情報の更新
        if self.steps % self.batch_size == 0:
            self.A_inv = np.copy(self._A_inv)  # インプレッション数(の逆行列)
            self.b = np.copy(self._b)  # クリック数


class LinTS(Agent):
    def __init__(self, n_arms=5, n_features=15, warm_up=1, batch_size=15,
                 noise=0.1):
        super().__init__(n_arms, n_features)
        self.theta_hat = np.zeros((n_features, n_arms))  # f*k
        self.noise = noise

        self.A_inv = np.concatenate(
            [np.identity(n_features) for i in np.arange(n_arms)]).reshape(n_arms,
                                                                          n_features,
                                                                          n_features)  # k*f*f
        self.b = np.zeros((n_features, n_arms))  # f*k
        self._A_inv = np.concatenate(
            [np.identity(n_features) for i in np.arange(n_arms)]).reshape(n_arms,
                                                                          n_features,
                                                                          n_features)  # k*f*f
        self._b = np.zeros((n_features, n_arms))  # f*k

    def initialize(self):
        super().initialize()
        self.theta_hat = np.zeros_like(self.theta_hat)  # f*k
        self.A_inv = np.concatenate(
            [np.identity(self.n_features) for i in np.arange(self.n_arms)]).reshape(
            self.n_arms, self.n_features, self.n_features)
        self.b = np.zeros_like(self.b)
        self._A_inv = np.concatenate(
            [np.identity(self.n_features) for i in np.arange(self.n_arms)]).reshape(
            self.n_arms, self.n_features, self.n_features)
        self._b = np.zeros_like(self._b)

    def select_arm(self, x):
        # x = np.expand_dims(x, axis=1)  # 特徴量の次元を変更
        avg = np.concatenate(
            [self.A_inv[i] @ np.expand_dims(self.b[:, i], axis=1) for i in
             np.arange(self.n_arms)],
            axis=1)  # f*k 正規分布のための平均値ベクトル
        scale = self.noise * self.A_inv  # k*d*d 正規分布のための分散共分散行列
        theta_hat = np.concatenate(
            [np.random.multivariate_normal(avg[:, i], scale[i]) for i in
             np.arange(self.n_arms)], axis=0)  # 多次元正規分布に従った正規乱数を生成
        theta_hat = np.reshape(theta_hat, [self.n_arms, self.n_features])
        result = np.argmax(x @ theta_hat.T)

        return result

    def update(self, chosen_arm=None, x=None, reward=None):
        super().update()
        self.counts[chosen_arm] += 1
        x = np.expand_dims(x, axis=1)  # 特徴量の次元を変更

        self._A_inv[chosen_arm] -= self._A_inv[chosen_arm] @ x @ x.T @ \
                                   self._A_inv[chosen_arm] / (
                                               1 + x.T @ self._A_inv[
                                           chosen_arm] @ x)
        self._b[:, chosen_arm] += np.ravel(x) * reward  # f*1
        # batch_size回ごとに情報の更新
        if self.steps % self.batch_size == 0:
            self.A_inv = np.copy(self._A_inv)
            self.b = np.copy(self._b)


class LinRS(Agent):
    def __init__(self, n_arms=5, n_features=15, warm_up=1, batch_size=300, aleph=0.8):
        super().__init__(n_arms, n_features)
        self.theta_hat = np.zeros((n_features, n_arms))  # f*k
        self.aleph = aleph  # 満足化基準値

        self.A_inv = np.concatenate(
            [np.identity(n_features) for i in np.arange(n_arms)]).reshape(n_arms,
                                                                          n_features,
                                                                          n_features)  # k*f*f
        self.b = np.zeros((n_features, n_arms))  # f*k
        self._A_inv = np.concatenate(
            [np.identity(n_features) for i in np.arange(n_arms)]).reshape(n_arms,
                                                                          n_features,
                                                                          n_features)  # k*f*f
        self._b = np.zeros((n_features, n_arms))  # f*k
        self.m = np.zeros((self.n_features, self.n_arms))
        self._m = np.zeros((self.n_features, self.n_arms))

    def initialize(self):
        super().initialize()
        self.theta_hat = np.zeros_like(self.theta_hat)  # f*k
        self.A_inv = np.concatenate(
            [np.identity(self.n_features) for i in np.arange(self.n_arms)]).reshape(
            self.n_arms, self.n_features, self.n_features)
        self.b = np.zeros_like(self.b)
        self._A_inv = np.concatenate(
            [np.identity(self.n_features) for i in np.arange(self.n_arms)]).reshape(
            self.n_arms, self.n_features, self.n_features)
        self._b = np.zeros_like(self._b)
        self.m = np.zeros_like(self.m)
        self._m = np.zeros_like(self._m)

    def select_arm(self, x):
        x = np.expand_dims(x, axis=1)  # 特徴量の次元を変更
        self.theta_hat = np.concatenate(
            [self.A_inv[i] @ np.expand_dims(self.b[:, i], axis=1) for i in
             np.arange(self.n_arms)], axis=1)  # f*k
        sigma_hat = np.concatenate(
            [self.A_inv[i] @ np.expand_dims(self.m[:, i], axis=1) for i in
             np.arange(self.n_arms)], axis=1)  # 1*k
        rs = sigma_hat.T @ x * (
                    self.theta_hat.T @ x - self.aleph)  # k*f @ f*1 * (k*f @ f*1 - 1) -> 1*k
        result = np.argmax(rs)

        return result

    def update(self, chosen_arm=None, x=None, reward=None):
        super().update()
        self.counts[chosen_arm] += 1
        x = np.expand_dims(x, axis=1)  # 特徴量の次元を変更

        self._A_inv[chosen_arm] -= self._A_inv[chosen_arm] @ x @ x.T @ \
                                   self._A_inv[chosen_arm] / (
                                           1 + x.T @ self._A_inv[
                                       chosen_arm] @ x)
        self._b[:, chosen_arm] += np.ravel(x) * reward  # f*1
        self._m[:, chosen_arm] += np.ravel(x)  # f*1
        # batch_size回ごとに情報の更新
        if self.steps % self.batch_size == 0:
            self.A_inv = np.copy(self._A_inv)
            self.b = np.copy(self._b)
            self.m = np.copy(self._m)
